#ifndef __HELPER_H
#define __HELPER_H

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <math.h>
#include <time.h>

#include <GL/gl.h>
#include <GL/glut.h>
#include <GL/glu.h>

#include "config.h"


void printFormatted(char *str) {
   int len = strlen(str);
   int i;
   for (i = 0; i < len; i++) {
      glutBitmapCharacter(GLUT_BITMAP_8_BY_13, *str++);
   }
}

void drawArea1(int x, int y) {

   char buffer[256];
   glColor3f(1.0, 1.0, 1.0);
   glRasterPos2i(x, y);
   memset(buffer, '\0', sizeof(buffer));
   sprintf(buffer, "a - rotate pyramid and sphere only");
   printFormatted(buffer);

   glColor3f(1.0, 1.0, 1.0);
   glRasterPos2i(x, y - 20);
   memset(buffer, '\0', sizeof(buffer));
   sprintf(buffer, "z - rotate cube and sphere only");
   printFormatted(buffer);   

   glColor3f(1.0, 1.0, 1.0);
   glRasterPos2i(x, y - 40);
   memset(buffer, '\0', sizeof(buffer));
   sprintf(buffer, "q - all objects stationary");
   printFormatted(buffer);   

   glColor3f(1.0, 1.0, 1.0);
   glRasterPos2i(x, y - 60);
   memset(buffer, '\0', sizeof(buffer));
   sprintf(buffer, "space - all objects rotate");
   printFormatted(buffer);   

   glColor3f(1.0, 1.0, 1.0);
   glRasterPos2i(x, y - 80);
   memset(buffer, '\0', sizeof(buffer));
   sprintf(buffer, "right mouse (hold) - change eye position to (5, 5, 5)");
   printFormatted(buffer);   

   glColor3f(1.0, 1.0, 1.0);
   glRasterPos2i(x, y - 100);
   memset(buffer, '\0', sizeof(buffer));
   sprintf(buffer, "left mouse (hold) - change eye position to (-5, 5, 5)");
   printFormatted(buffer);   
}

void drawArea2(int x, int y) {
   
   char buffer[256];   
   glColor3f(1.0, 1.0, 1.0);
   glRasterPos2i(x, y - 20);
   memset(buffer, '\0', sizeof(buffer));
   sprintf(buffer, "eye:");
   printFormatted(buffer);

   glColor3f(1.0, 1.0, 1.0);
   glRasterPos2i(x, y - 35);
   memset(buffer, '\0', sizeof(buffer));
   sprintf(buffer, "(%.1f, %.1f, %.1f)", lx, ly, lz);
   printFormatted(buffer);
}

void drawInfo() {

   glDisable(GL_TEXTURE_2D);
   
   glMatrixMode(GL_PROJECTION);
   glPushMatrix();
   glLoadIdentity();

   gluOrtho2D(0.0, windowWidth, 0.0, windowHeight);
   glMatrixMode(GL_MODELVIEW);
   glPushMatrix();
   glLoadIdentity();
   
   drawArea1(50, windowHeight - 30);
   drawArea2(10, windowWidth / 2);

   glMatrixMode(GL_PROJECTION);
   glPopMatrix();
   glMatrixMode(GL_MODELVIEW);
   glPopMatrix();

   glEnable(GL_TEXTURE_2D);
}

#endif
