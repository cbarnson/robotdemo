INCLUDE = -I/usr/include/
LIBDIR  = -L/usr/lib/

COMPILERFLAGS = -Wall
CC = g++
CFLAGS = $(COMPILERFLAGS) $(INCLUDE)
LIBRARIES = -lX11 -lXi -lglut -lGL -lGLU -lm

all: main

main: main.o
	$(CC) $(CFLAGS) -o $@ $(LIBDIR) $< $(LIBRARIES) helper.h config.h robot.h

.PHONY: clean

clean: 
	rm -f main *~ *% *# .#* *.o *.d
