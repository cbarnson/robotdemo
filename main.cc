// CPSC 3710 - Assignment 3 - Question 3
// Cody Barnson
// CPSC3710 - Assignment 3 - Question 4
// mulrotate.c

#include "config.h"
#include "helper.h"
#include "robot.h"

#include <math.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>

//============================================================
// NOTE: to set starting eye position (lx, ly, lz) edit the variables
// in header file "config.h"
//============================================================

// OFFSET FOR CUBE AND PYRAMID ALONG Z-AXIS
// positional information
float cubez = 0;
float pyrz = 1.0;

// flags for spin status
int isSpinCube = 1;
int isSpinPyramid = 1;
int isSpinSphere = 1;

// angle for each spin
int angleCube = 0;
int anglePyramid = 0;
int angleSphere = 0;
int angleSpinDelta = 5;

float sphereRadius = 0.5;

// copy of initial value for z component of eye position (needed
// to be able to reset eye position after mouse button is released)
float lz_original;

GLfloat color[6][3] = {
    {1.0, 1.0, 0.0},  // yellow
    {1.0, 0.0, 0.0},  // red
    {0.0, 1.0, 0.0},  // green
    {0.0, 0.0, 1.0},  // blue
    {0.0, 1.0, 1.0},  // light blue
    {1.0, 0.0, 1.0}   // pink
};

GLfloat cube[8][3] = {{0.5, 0.5, 0.5},    {-0.5, 0.5, 0.5}, {-0.5, -0.5, 0.5},
                      {0.5, -0.5, 0.5},   {0.5, 0.5, -0.5}, {-0.5, 0.5, -0.5},
                      {-0.5, -0.5, -0.5}, {0.5, -0.5, -0.5}};

GLfloat pyramid[5][3] = {{0.5, -0.5, 0.5},
                         {0.5, -0.5, -0.5},
                         {-0.5, -0.5, -0.5},
                         {-0.5, -0.5, 0.5},
                         {0.0, 0.5, 0.0}};

// function to call draw for cube, pyramid, and sphere
void drawObjects() { drawRobot(); }

// sets the projection
void setProjection() {
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  gluPerspective(fov, asp, dim / 4, 4 * dim);
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();
}

void setViewPosition() { gluLookAt(lx, ly, lz, 0, 0, 0, 0, 1, 0); }

// coordinate system axes
void drawAxes() {
  // length of each axis to draw
  double len = 2.0;
  glColor3f(1.0, 1.0, 1.0);
  glBegin(GL_LINES);
  glVertex3d(0, 0, 0);
  glVertex3d(len, 0, 0);
  glVertex3d(0, 0, 0);
  glVertex3d(0, len, 0);
  glVertex3d(0, 0, 0);
  glVertex3d(0, 0, len);
  glEnd();
}

// render scene
void display() {
  // clear color and depth buffers for new rendering
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  // turn on culling
  glEnable(GL_CULL_FACE);
  glEnable(GL_DEPTH_TEST);
  glLoadIdentity();
  setViewPosition();

  // some xyz axes to make it clear what is going on
  drawAxes();
  drawObjects();

  // draw some useful information about key functions and eye position
  drawInfo();
  glutSwapBuffers();
}

void windowResize(int width, int height) {
  asp = (height > 0) ? (double)width / height : 1;
  glViewport(0, 0, width, height);
  setProjection();
}

void keyboardUp(unsigned char key, int x, int y) {
  switch (key) {
    case 'r':
      angleRobotHead = angleFaceForward;
      break;
    case 'l':
      angleRobotHead = angleFaceForward;
      break;
    default:
      break;
  }
  setProjection();
  glutPostRedisplay();
}

void keyboard(unsigned char key, int x, int y) {
  switch (key) {
    case 27:  // exit
      exit(0);
    case 'w':
      moveRobotForward();
      break;
    case 'd':  // rotate 'right'
      turnRobotRight();
      break;
    case 'a':  // rotate 'left'
      turnRobotLeft();
      break;
    case 'r':
      angleRobotHead = angleFaceRight;
      break;
    case 'l':
      angleRobotHead = angleFaceLeft;
      break;
    default:
      break;
  }
  setProjection();
  glutPostRedisplay();
}

void mouse(int button, int state, int x, int y) {
  switch (button) {
    case GLUT_RIGHT_BUTTON:
      if (state == GLUT_DOWN) {
        lx = 5;
        ly = 5;
        lz = 5;
      } else if (state == GLUT_UP) {
        lx = 0;
        ly = 0;
        lz = lz_original;
      }
      break;
    case GLUT_LEFT_BUTTON:
      if (state == GLUT_DOWN) {
        lx = -5;
        ly = 5;
        lz = 5;
      } else if (state == GLUT_UP) {
        lx = 0;
        ly = 0;
        lz = lz_original;
      }
      break;
    default:
      return;
  }
  setProjection();
  glutPostRedisplay();
}

int main(int argc, char* argv[]) {
  lz_original = lz;

  glutInit(&argc, argv);

  // initialize depth buffer and enable hidden surface removal
  glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE | GLUT_DEPTH);
  glutInitWindowSize(windowWidth, windowHeight);
  char* windowName = "Cody Barnson";
  glutCreateWindow(windowName);
  glutDisplayFunc(display);
  glutReshapeFunc(windowResize);
  glutKeyboardFunc(keyboard);
  glutKeyboardUpFunc(keyboardUp);
  glutMouseFunc(mouse);

  glutSetKeyRepeat(GLUT_KEY_REPEAT_OFF);

  glutMainLoop();
  return 0;
}
