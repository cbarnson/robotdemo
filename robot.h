#ifndef ROBOT_H
#define ROBOT_H

#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>

#include <cmath>

// enables turning
bool robotAtIntersection = true;

int DIRECTION[4] = {0, -90, -180, -270};
int robotDirection = 0;

float delta = 0.2;
float dx[4] = {0, delta, 0, -delta};
float dy[4] = {0, 0, 0, 0};
float dz[4] = {-delta, 0, delta, 0};

float rx = 0, ry = 0, rz = 0;
int angleRobot = 0;
int angleRobotHead = 0;

int angleFaceLeft = 45;
int angleFaceRight = -45;
int angleFaceForward = 0;

GLfloat origin[3] = {0.0, -0.5, 0.0};
GLfloat baseHeight = 1.5;
GLfloat baseWidth = 0.5;

GLfloat originHead[3] = {origin[0] + baseHeight, origin[1] + baseHeight,
                         origin[2] + baseHeight};
GLfloat headHeight = 0.5;
GLfloat headWidth = 0.25;

GLfloat base[8][3] = {{baseWidth, origin[1] + baseHeight, baseWidth},
                      {-baseWidth, origin[1] + baseHeight, baseWidth},
                      {-baseWidth, origin[1], baseWidth},
                      {baseWidth, origin[1], baseWidth},
                      {baseWidth, origin[1] + baseHeight, -baseWidth},
                      {-baseWidth, origin[1] + baseHeight, -baseWidth},
                      {-baseWidth, origin[1], -baseWidth},
                      {baseWidth, origin[1], -baseWidth}};

GLfloat head[8][3] = {{headWidth, originHead[1] + headHeight, headWidth},
                      {-headWidth, originHead[1] + headHeight, headWidth},
                      {-headWidth, originHead[1], headWidth},
                      {headWidth, originHead[1], headWidth},
                      {headWidth, originHead[1] + headHeight, -headWidth},
                      {-headWidth, originHead[1] + headHeight, -headWidth},
                      {-headWidth, originHead[1], -headWidth},
                      {headWidth, originHead[1], -headWidth}};

void initRobot();
void drawRobotHead();
void drawRobotBase();
void moveRobotForward();
void turnRobotRight();
void turnRobotLeft();
bool robotInBound(float x, float y, float z);

bool robotInBound(float x, float y, float z) { return true; }

// if the robot is at a location where it is valid to turn (intersection),
// then update the robotDirection index (relative to the DIRECTION array),
// and write the new value for angleRobot respective to this new value
void turnRobotRight() {
  if (!robotAtIntersection) return;
  robotDirection = (robotDirection + 1) % 4;
  angleRobot = DIRECTION[robotDirection];
}

// if the robot is at a location where it is valid to turn (intersection),
// then update the robotDirection index (relative to the DIRECTION array),
// and write the new value for angleRobot respective to this new value
void turnRobotLeft() {
  if (!robotAtIntersection) return;
  robotDirection = (robotDirection + (4 - 1)) % 4;
  angleRobot = DIRECTION[robotDirection];
}

// using current direction, computes the resulting position of the robot
// if this position is in bound, new position is written to robot's
// coordinates
void moveRobotForward() {
  int i = robotDirection;
  float x = rx + dx[i];
  float y = ry + dy[i];
  float z = rz + dz[i];
  if (robotInBound(x, y, z)) {
    rx = x;
    ry = y;
    rz = z;
  }
}

void drawRobot() {
  glPushMatrix();
  glTranslated(rx, ry, rz);
  glRotated(angleRobot, 0, 1, 0);

  // draw robot head and rotate based on parameters set by keypresses
  glPushMatrix();
  glRotated(angleRobotHead, 0, 1, 0);
  drawRobotHead();
  glPopMatrix();

  // draw robot base, at the position specified by rx ry rz, this
  // also moves the head with it
  drawRobotBase();
  glPopMatrix();
}

void drawRobotBase() {
  float front = 0.7;
  float fact = 0.6;
  float offy = 0.2;

  // for triangles to be drawn on back of robot
  GLfloat tri[3][3] = {
      {base[2][0] * fact, base[2][1] + offy, base[2][2] * 1.001},
      {base[3][0] * fact, base[3][1] + offy, base[3][2] * 1.001},
      {0.0, base[1][1] * 0.25, base[3][2] * 1.001}};

  // 6 cube faces
  glBegin(GL_QUADS);
  // yellow - back of the robot
  glColor3f(1.0, 1.0, 0.0);
  glVertex3fv(base[0]);
  glVertex3fv(base[1]);
  glVertex3fv(base[2]);
  glVertex3fv(base[3]);
  glEnd();
  // red - inner triangles on the robot
  glBegin(GL_TRIANGLES);
  // bottom triangle
  glColor3f(1.0, 0.0, 0.0);
  glVertex3fv(tri[0]);
  glVertex3fv(tri[1]);
  glVertex3fv(tri[2]);
  // top triangle
  float off2 = 0.55;
  glVertex3f(tri[0][0], tri[0][1] + off2, tri[0][2]);
  glVertex3f(tri[1][0], tri[1][1] + off2, tri[1][2]);
  glVertex3f(tri[2][0], tri[2][1] + off2, tri[2][2]);

  glEnd();

  glBegin(GL_QUADS);
  // red - front of the robot
  glColor3f(1.0, 0.0, 0.0);
  glVertex3fv(base[5]);
  glVertex3fv(base[4]);
  glVertex3fv(base[7]);
  glVertex3fv(base[6]);
  // green - inner rect on front
  glColor3f(0.0, 1.0, 0.0);
  glVertex3f(base[5][0] * front, base[5][1] * front, base[5][2] * 1.001);
  glVertex3f(base[4][0] * front, base[4][1] * front, base[4][2] * 1.001);
  glVertex3f(base[7][0] * front, base[7][1] * front, base[7][2] * 1.001);
  glVertex3f(base[6][0] * front, base[6][1] * front, base[6][2] * 1.001);

  // green
  glColor3f(0.0, 1.0, 0.0);
  glVertex3fv(base[4]);
  glVertex3fv(base[0]);
  glVertex3fv(base[3]);
  glVertex3fv(base[7]);
  // blue
  glColor3f(0.0, 0.0, 1.0);
  glVertex3fv(base[1]);
  glVertex3fv(base[5]);
  glVertex3fv(base[6]);
  glVertex3fv(base[2]);
  // light blue
  glColor3f(0.0, 1.0, 1.0);
  glVertex3fv(base[4]);
  glVertex3fv(base[5]);
  glVertex3fv(base[1]);
  glVertex3fv(base[0]);
  // pink
  glColor3f(1.0, 0.0, 1.0);
  glVertex3fv(base[3]);
  glVertex3fv(base[2]);
  glVertex3fv(base[6]);
  glVertex3fv(base[7]);
  glEnd();
}

// TODO: add cylinder eyes and antenna that rotates
void drawRobotHead() {
  // 6 cube faces
  glBegin(GL_QUADS);
  // green - back of the robot head
  glColor3f(0.0, 1.0, 0.0);
  glVertex3fv(head[0]);
  glVertex3fv(head[1]);
  glVertex3fv(head[2]);
  glVertex3fv(head[3]);

  // red - front of the robot face
  glColor3f(1.0, 0.0, 0.0);
  glVertex3fv(head[5]);
  glVertex3fv(head[4]);
  glVertex3fv(head[7]);
  glVertex3fv(head[6]);
  // pink
  glColor3f(1.0, 0.0, 1.0);
  glVertex3fv(head[4]);
  glVertex3fv(head[0]);
  glVertex3fv(head[3]);
  glVertex3fv(head[7]);
  // blue
  glColor3f(0.0, 0.0, 1.0);
  glVertex3fv(head[1]);
  glVertex3fv(head[5]);
  glVertex3fv(head[6]);
  glVertex3fv(head[2]);
  // light blue
  glColor3f(0.0, 1.0, 1.0);
  glVertex3fv(head[4]);
  glVertex3fv(head[5]);
  glVertex3fv(head[1]);
  glVertex3fv(head[0]);
  // pink
  glColor3f(1.0, 0.0, 1.0);
  glVertex3fv(head[3]);
  glVertex3fv(head[2]);
  glVertex3fv(head[6]);
  glVertex3fv(head[7]);
  glEnd();
}

#endif