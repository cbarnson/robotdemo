#ifndef __CONFIG_H
#define __CONFIG_H

int windowWidth = 500;
int windowHeight = 450;

// orthogonal box dimension
double dim=5.0; 
// azimuth
int th = 20;  
// elevation
int ph = 25;  
// field of view
int fov = 55; 
// aspect ratio
int asp = 1; 

// LookAt x y z
float lx = 0, ly = 0, lz = 5;

#endif
